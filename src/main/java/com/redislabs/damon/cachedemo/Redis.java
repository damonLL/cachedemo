package com.redislabs.damon.cachedemo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class Redis {

	public Jedis jedis = null;
	private JedisPool pool = null;
	
	/**
	 * Creates a complete connection to Redis.
	 * <p>
	 *
	 * @param  hostname   IP Address or hostname of Redis server.
	 * @param  port       The port to connect to Redis (typically 6379).
	 */
	
	public Redis () {

	}
	
	public void connect (HashMap<String,String> args) {
		
		String redisHostname = args.get("redisHostname");
		int redisPort = Integer.parseInt(args.get("redisPort"));
		pool = new JedisPool(new JedisPoolConfig(), redisHostname, redisPort);;

		try {
			jedis = pool.getResource();
		} catch (redis.clients.jedis.exceptions.JedisConnectionException re) {
			System.out.println("Can't connect to Redis. Is it running on " +
					redisHostname + ":" + redisPort + " ?");
			System.exit(1);;
		} catch (Exception e) {
			System.out.println("Redis Exception:" + e.toString());
			System.exit(1);
		}
		
	}
	
	public void setList(String key, List<Object> result) {
		String[] values = result.toArray(new String[0]);
		jedis.lpush(key, values);
	}
	
	public void closeRedis() {
		this.pool.close();
	}
		
	public boolean keyExists(String key) {
		return jedis.exists(key);
	}
	
	public List<Object> getList(String key) {
		return new ArrayList<Object>(jedis.lrange(key, 0, -1));
	}
	
	public String getStringKey(String key) {
		return jedis.get(key);
	}
	
	public int getIntegerKey(String key) {
		return Integer.valueOf(jedis.get(key));
	}
	
	public Map<String,String> getHashKey(String key) {
		return jedis.hgetAll(key);
	}
	
	public Set<String> getSetKey(String key) {
		return jedis.smembers(key);
	}
	
	public Jedis getJedis() {
		return this.jedis;
	}
	
	public void setJedis(Jedis jedis) {
		this.jedis = jedis;
	}
	
}

package com.redislabs.damon.cachedemo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * CachingExample
 *
 * @author damonlrr
 */
public class App {
	
	public static final List<String> STATES = new ArrayList<String>(Arrays.asList("AK", "AL", "AR", "AZ", "CA", "CO",
			"CT", "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MI", "MN", "MO",
			"MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN",
			"TX", "UT", "VA", "VT", "WA", "WI", "WV", "WY"));
		
	public static Boolean USE_CACHE = false;
	
	// Which tests to run
	
	public static Boolean TEST_PATIENTS_PER_STATE = true;
	public static Boolean TEST_PATIENTS_PER_NAME = true;
	
	public static void main(String[] args) {
		
		ArgParser argParser = new ArgParser(args);	
		HashMap<String, String> parsedArgs = argParser.parseArgs(args);
		
		// first argument is source datatabase
		// second argument contains HashMap of *all* connection parameters (including Redis if selected)
		// third argument is whether to use caching w/ redis

		DataLayer dl = new DataLayer("mongodb", parsedArgs, true);

		// Get current time
		long startMs = System.currentTimeMillis();
		
		// Make 50 states call
		if (TEST_PATIENTS_PER_STATE) testPatientsPerState(dl);
		
		// Get delta between current system time and last system time
		long diff = System.currentTimeMillis() - startMs;
		
		System.out.println("total time:" + Long.toString(diff));
	}

	public static void testPatientsPerState(DataLayer dl) {
		
		for (String state : STATES) {
			
			HashMap<String,String> queryFor = new HashMap<String,String>();
			queryFor.put("state", state);
			
			ArrayList<String> returnFields = new ArrayList<String>();
			returnFields.add("_id");
			
			List<Object> result = dl.executeQuery(queryFor, returnFields);
			System.out.print(state + ":");
			System.out.println(result.size());

		}
	}
	
	public static void testPatientsPerName(DataLayer dl) {
		// read in the text files here and iterate through the first names
	}

}

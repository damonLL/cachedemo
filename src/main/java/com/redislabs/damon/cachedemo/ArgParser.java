package com.redislabs.damon.cachedemo;

import java.io.PrintWriter;
import java.util.HashMap;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;

public class ArgParser {

	private static CommandLine commandLine = null;
	private static CommandLineParser parser = null;
	private static Options allOptions = new Options();
	public static HashMap<String,String> DEFAULTS = new HashMap<String,String>();
	private static Boolean VERBOSE = false;
	public static Option helpOption = Option.builder("h")
			.longOpt("help")
			.required(false)
			.hasArg(false)
			.build();
	public static Option verboseOption = Option.builder("v")
			.longOpt("verbose")
			.required(false)
			.hasArg(false)
			.build();

	public ArgParser (String[] args) {
		buildDefaults();
		buildOptions();
		setupOptions(args);
		parser = new DefaultParser();
		
		try {
			commandLine = parser.parse(allOptions, args);
		}
		catch (org.apache.commons.cli.UnrecognizedOptionException ex) {
			ex.printStackTrace();
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public void printHelp() {
		// Good example: https://self-learning-java-tutorial.blogspot.com/2016/12/commons-cli-optiongroup-group-mutually.html
		PrintWriter writer = new PrintWriter(System.out);
		final HelpFormatter helpFormatter = new HelpFormatter();
		helpFormatter.printHelp(writer, 80, "CacheExample", "", allOptions, 5, 5, "true", true);
		writer.flush();
	}

	public static void buildOptions() {
		
		// GLOBAL OPTIONS
		
		allOptions.addOption(helpOption);
		allOptions.addOption(verboseOption);
		
		// REDIS OPTIONS
		
		Option redisHostname = new Option("n", "redisHostname", true, "Redis Hostname");
		redisHostname.setRequired(false);
		redisHostname.setArgName("REDIS HOST");
		
		Option redisPort = new Option("p", "redisPort", true, "Redis Port");
		redisPort.setRequired(false);
		redisPort.setArgName("REDIS PORT");
		
		allOptions.addOption(redisHostname);
		allOptions.addOption(redisPort);
		
		// MONGO OPTIONS
		Option mongoHostname = new Option("s", "mongoHostname", true, "MongoDB Hostname");
		mongoHostname.setRequired(false);
		mongoHostname.setArgName("MONGO HOST");
		
		Option mongoPort = new Option("r", "mongoPort", true, "MongoDB Port");
		mongoPort.setRequired(false);
		mongoPort.setArgName("MONGO PORT");
		
		Option mongoDatabase = new Option("b", "mongoDatabase", true, "MongoDB Database");
		mongoDatabase.setRequired(false);
		mongoDatabase.setArgName("MONGO DATABASE");
		
		Option mongoCollection = new Option("c", "mongoCollection", true, "MongoDB Collection");
		mongoCollection.setRequired(false);
		mongoCollection.setArgName("MONGO COLLECTION");
		
		allOptions.addOption(mongoHostname);
		allOptions.addOption(mongoPort);
		allOptions.addOption(mongoDatabase);
		allOptions.addOption(mongoCollection);
		
	}
	
	public static void buildDefaults() {
		
		DEFAULTS.put("redisHostname", "localhost");
		DEFAULTS.put("redisPort", "6379");
		DEFAULTS.put("mongoHostname", "localhost");
		DEFAULTS.put("mongoPort", "27017");
		DEFAULTS.put("mongoDatabase", "data");
		DEFAULTS.put("mongoCollection", "patients");
		
	}

	public static boolean checkForVerbose(String[] args) throws ParseException {

		Options options = new Options();

		try {
			options.addOption(verboseOption);
			
			CommandLineParser parser = new DefaultParser();

			CommandLine cmd = parser.parse(options, args);

			if (cmd.hasOption(verboseOption.getOpt())) {
				VERBOSE = true;
			}

		} catch (ParseException e) { }

		return VERBOSE;
	}
	
	public static boolean checkForHelp(String[] args) throws ParseException {
		boolean hasHelp = false;

		Options options = new Options();

		try {
			options.addOption(helpOption);
			
			CommandLineParser parser = new DefaultParser();

			CommandLine cmd = parser.parse(options, args);

			if (cmd.hasOption(helpOption.getOpt())) {
				hasHelp = true;
			}

		} catch (ParseException e) { }

		return hasHelp;
	}
	  
	public static void setupOptions(String[] args) {
		
		allOptions.addOption(verboseOption);
		allOptions.addOption(helpOption);

		try {
			if (checkForHelp(args)) {
				HelpFormatter fmt = new HelpFormatter();
				fmt.printHelp("CacheExample", allOptions);
				System.exit(0);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		try {
			if (checkForVerbose(args)) {
				VERBOSE = true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	public void displayArgs(HashMap<String, String> args) {
		for (HashMap.Entry<String, String> entry : args.entrySet()) {
			System.out.println(entry.getKey() + " : " + entry.getValue());
		}
	}
	
	public HashMap<String, String> parseArgs(String[] args) {
		
		HashMap<String, String> returnedArgs = new HashMap<String,String>();
		
		returnedArgs.put("help", processArg("help", "h", DEFAULTS, args));
		returnedArgs.put("verbose", processArg("verbose", "v", DEFAULTS, args));
		returnedArgs.put("mongoHostname", processArg("mongoHostname", "s", DEFAULTS, args));
		returnedArgs.put("mongoPort", processArg("mongoPort", "r", DEFAULTS, args));
		returnedArgs.put("mongoDatabase", processArg("mongoDatabase", "b", DEFAULTS, args));
		returnedArgs.put("mongoCollection", processArg("mongoCollection", "c", DEFAULTS, args));
		returnedArgs.put("redisHostname", processArg("redisHostname", "n", DEFAULTS, args));
		returnedArgs.put("redisPort", processArg("redisPort", "p", DEFAULTS, args));
		
		return returnedArgs;

	}
	
	public static String processArg(String optionName, String optionLetter, HashMap<String, String> defaults, String[] args) {
		
		String returnValue = null;
		
		if (VERBOSE) System.out.println("Looking for optionName:" + optionName + " optionLetter:" + optionLetter);
		
			if (commandLine.hasOption(optionLetter)) {
				returnValue = commandLine.getOptionValue(optionLetter);
				if (VERBOSE) System.out.println("Found in command line options: " + returnValue);
			} else {
				if (defaults.containsKey(optionName)) {
					returnValue = defaults.get(optionName);
					if (VERBOSE) System.out.println("Found in defaults: " + returnValue);
				}
		}
		
		return returnValue;
		
	}

}

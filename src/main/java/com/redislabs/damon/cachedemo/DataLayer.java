package com.redislabs.damon.cachedemo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;

/* this class abstracts the connection, caching, and querying
 * of all DB connections
 */

public class DataLayer {

	public Boolean USE_CACHE = false;
	private Redis r = null;
	private SourceDatabase s = null;
	
	public DataLayer(String sourceDb, HashMap<String, String> args, boolean useCache) {
				
		r = new Redis();

		if (sourceDb.equalsIgnoreCase("mongodb")) {
			s = new MongoDB();
			s.connect(args);
		}
		
		if (sourceDb.equalsIgnoreCase("mysql")) {
			s = new MySQL();
			s.connect(args);
		}
		
		if (useCache) {
			r.connect(args);
			USE_CACHE = useCache;
		}
		
	}

	public static void main(String[] args) {
		
	}
	
	public List<Object> executeQuery(HashMap<String,String> queryFilter, ArrayList<String> returnFields) {
		
		// If Redis is available, then create an MD5 hash of the query
		// and use it to search within Redis. If Redis is not available,
		// there is no reason to calculate the MD5 hash.
		
		List<Object> result = new ArrayList<Object>();
		String md5 = null;

		if (USE_CACHE) {
			
			// Use a StringBuilder instead of String because as we add each
			// filter to the string so that we can create the hash, it doesn't
			// have to copy the previous value.
			StringBuilder queryString = new StringBuilder();
			
			for (Map.Entry<String, String> filter : queryFilter.entrySet()) {
				//System.out.println("filter:" + filter.toString());
				queryString = queryString.append(filter.getKey());
				queryString = queryString.append(filter.getValue());
			}
			
			Iterator<String> fieldsIterator = returnFields.iterator();
			while (fieldsIterator.hasNext()) {
				queryString = queryString.append(fieldsIterator.next());
			}
			
			md5 = getMd5Hash(queryString.toString());

		}
		
		if (! USE_CACHE) {
			// Not using cache, return directly from sourceDB
			result = s.query(queryFilter, returnFields);
		} else {
			// Check to see if value is in cache.
			// If it is, return the value.
			// If it is not:
			//    1. Fetch value from SourceDB
			//    2. Save key (md5 hash) and value in Redis
			//    3. Return value
			if (r.keyExists(md5)) {
				result = r.getList(md5);
			} else {
				result = s.query(queryFilter, returnFields);
				r.setList(md5, result);
			}
		}
		return result;
	}
	
	public static String getMd5Hash(String query) {
		
		String md5 = DigestUtils.md5Hex(query);
		return md5;

	}
	
}

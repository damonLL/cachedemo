package com.redislabs.damon.cachedemo;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bson.Document;

/**
 * Object that manages all of the connection and communications
 * between the primary application and MongoDB
 * 
 * @author damonlrr
 * 
 */
public class MongoDB extends SourceDatabase {
		
	private MongoClient mongoClient = null;
	private MongoDatabase mongoDatabase = null;
	private MongoCollection<Document> mongoCollection = null;
	
	/**
	 * Creates a complete connection to MongoDB.
	 * <p>
	 *
	 * @param  hostname   IP Address or hostname of MongoDB server.
	 * @param  port       The port to connect to MongoDB (typically 27017).
	 * @param  database   The name of the database within MongoDB to use.
	 * @param  collection The name of the collection within the database to use.
	 */
	
	public MongoDB () {

	}
	
	public void connect (HashMap<String,String> args) {
		
		try {
			
			this.mongoClient = MongoClients.create("mongodb://" +
					args.get("mongoHostname") +
					":" +
					Integer.parseInt(args.get("mongoPort")));
			this.mongoDatabase = this.mongoClient.getDatabase(args.get("mongoDatabase"));
			this.mongoCollection = this.mongoDatabase.getCollection(args.get("mongoCollection"));
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
		}
		
	}
	/**
	 * Queries MongoDB based on the filter provided.
	 * 
	 * @param filter A Document containing one or more filters
	 * @return A simple ArrayList containing the results from MongoDB
	 * 
	 */
	
	public List<Object> query(HashMap<String,String> filters, List<String> returnFields) {
		
		List<Object> response = new ArrayList<Object>();
		Document allFilters = new Document();
		allFilters.putAll(filters);

		FindIterable<Document> result =
				this.mongoCollection.find(allFilters).projection(Projections.include(returnFields));

		for (Document currentDoc : result) {
			String id = currentDoc.getString("_id");
			response.add(id);
		}
		return response;
		
	}

}

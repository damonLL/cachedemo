package com.redislabs.damon.cachedemo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Object that manages all of the connection and communications
 * between the primary application and MySQL
 * 
 * @author damonlrr
 * 
 */
public class MySQL extends SourceDatabase {
		
	public MySQL () {

	}
	
	public void connect (HashMap<String,String> args) {
		
	}
	
	/**
	 * Queries MySQL based on the filter provided.
	 */
	
	public List<Object> query(HashMap<String,String> filters, List<String> returnFields) {
		
		return new ArrayList<Object>();
		
	}

}

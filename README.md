# README #

CacheDemo

### What is this repository for? ###

The intention of this code is to provide an example for using Redis
as a cache in front of one or more databases. Currently it supports
MongoDB and a very specific dataset. In the future it will provide
additional flexibility.

### How do I get set up? ###

Clone the repo and build with your favorite Java IDE. Make sure
to have Redis running and MongoDB. If you need the example data
set I use (for the tests) please contact me, it's large and
has phony patient data that some might mistake as real.

### Contribution guidelines ###


### Who do I talk to? ###
